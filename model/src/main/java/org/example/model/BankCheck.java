package org.example.model;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.time.LocalDate;

// таблица для конкретного класса с неявным полиморфизмом
//@Entity

// таблица для конкретного класса с union
//@Entity

// таблица для каждого подкласса
//@Entity

// таблица для иерархии классов
@Entity
@DiscriminatorValue("bank_check")
public class BankCheck extends BillingDetails {

    @Column
    private String owner;

    @Column
    private Long account;

    public BankCheck(String fullName, String address, LocalDate created, String owner, Long account) {
        super(fullName, address, created);
        this.owner = owner;
        this.account = account;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public Long getAccount() {
        return account;
    }

    public void setAccount(Long account) {
        this.account = account;
    }
}
