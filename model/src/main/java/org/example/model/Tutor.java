package org.example.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Tutor {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private String title;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "contact_id")
    private Contact contact;

    @OneToMany(mappedBy = "lecturer", fetch = FetchType.LAZY)
    private List<CourseClass> lecturerCourseClasses = new ArrayList<>();

    @OneToMany(mappedBy = "trainer", fetch = FetchType.LAZY)
    private List<CourseClass> trainerCourseClasses = new ArrayList<>();

    public Tutor() {
    }

    public Tutor(String title) {
        this.title = title;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Contact getContact() {
        return contact;
    }

    public void setContact(Contact contact) {
        this.contact = contact;
    }

    public List<CourseClass> getLecturerCourseClasses() {
        return lecturerCourseClasses;
    }

    public void setLecturerCourseClasses(List<CourseClass> lecturerCourseClasses) {
        this.lecturerCourseClasses = lecturerCourseClasses;
    }

    public List<CourseClass> getTrainerCourseClasses() {
        return trainerCourseClasses;
    }

    public void setTrainerCourseClasses(List<CourseClass> trainerCourseClasses) {
        this.trainerCourseClasses = trainerCourseClasses;
    }
}
