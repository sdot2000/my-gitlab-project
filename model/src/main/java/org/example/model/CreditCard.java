package org.example.model;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.time.LocalDate;

// таблица для конкретного класса с неявным полиморфизмом
//@Entity

// таблица для конкретного класса с union
//@Entity

// таблица для каждого подкласса
//@Entity

// таблица для иерархии классов
@Entity
@DiscriminatorValue("credit_card")
public class CreditCard extends BillingDetails {

    @Column
    private Long number;

    @Column
    private LocalDate expiredIn;

    @Column
    private String cardholder;

    @Column
    private Integer cvv;

    public CreditCard(String fullName, String address, LocalDate created, Long number, LocalDate expiredIn, String cardholder, Integer cvv) {
        super(fullName, address, created);
        this.number = number;
        this.expiredIn = expiredIn;
        this.cardholder = cardholder;
        this.cvv = cvv;
    }

    public Long getNumber() {
        return number;
    }

    public void setNumber(Long number) {
        this.number = number;
    }

    public LocalDate getExpiredIn() {
        return expiredIn;
    }

    public void setExpiredIn(LocalDate expiredIn) {
        this.expiredIn = expiredIn;
    }

    public String getCardholder() {
        return cardholder;
    }

    public void setCardholder(String cardholder) {
        this.cardholder = cardholder;
    }

    public Integer getCvv() {
        return cvv;
    }

    public void setCvv(Integer cvv) {
        this.cvv = cvv;
    }
}
