package org.example.model;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class UserInfo {

    @Column
    private String firstName;

    @Column
    private String lastName;

    public UserInfo(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
}
