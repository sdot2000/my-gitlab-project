package org.example.model;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.MapsId;
import javax.persistence.OneToOne;
import java.util.ArrayList;
import java.util.List;

@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@Entity
public class Student {

    @Id
    @GenericGenerator(name = "one-one", strategy = "foreign",
            parameters = @Parameter(name = "property", value = "contact"))
    @GeneratedValue(generator = "one-one")
    @Column(name = "contact_id")
    private Long id;

    @Column
    private Long studentIdentifier;

    @OneToOne(fetch = FetchType.LAZY)
    @MapsId
    //@PrimaryKeyJoinColumn(name = "contact_id")
    private Contact contact;

    @ManyToMany(mappedBy = "students", fetch = FetchType.LAZY)
    private List<CourseClass> courseClasses = new ArrayList<>();

    public Student() {
    }

//    public Long getId() {
//        return id;
//    }
//
//    public void setId(Long id) {
//        this.id = id;
//    }

    public Long getStudentIdentifier() {
        return studentIdentifier;
    }

    public void setStudentIdentifier(Long studentIdentifier) {
        this.studentIdentifier = studentIdentifier;
    }

    public Contact getContact() {
        return contact;
    }

    public void setContact(Contact contact) {
        this.contact = contact;
    }

    public List<CourseClass> getCourseClasses() {
        return courseClasses;
    }

    public void setCourseClasses(List<CourseClass> courseClasses) {
        this.courseClasses = courseClasses;
    }
}
