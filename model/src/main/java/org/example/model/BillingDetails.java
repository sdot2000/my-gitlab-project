package org.example.model;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.MappedSuperclass;
import java.time.LocalDate;

// таблица для конкретного класса с неявным полиморфизмом
//@MappedSuperclass

// таблица для конкретного класса с union
//@Entity
//@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)

// таблица для каждого подкласса
//@Entity
//@Inheritance(strategy = InheritanceType.JOINED)

// таблица для иерархии классов
@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "billing_type", discriminatorType = DiscriminatorType.STRING)
public abstract class BillingDetails {

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    private Long id;

    @Column
    private String fullName;

    @Column
    private String address;

    @Column
    private LocalDate created;

    public BillingDetails(String fullName, String address, LocalDate created) {
        this.fullName = fullName;
        this.address = address;
        this.created = created;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public LocalDate getCreated() {
        return created;
    }

    public void setCreated(LocalDate created) {
        this.created = created;
    }
}
