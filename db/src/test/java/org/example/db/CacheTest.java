package org.example.db;

import org.example.model.Contact;
import org.hibernate.Session;
import org.hibernate.jpa.QueryHints;
import org.hibernate.query.Query;
import org.junit.Test;

import java.util.List;

public class CacheTest extends AbstractHibernateTest {

    @Test
    public void testLvl1Cache() {
        try (final Session session = getSessionFactory().openSession()){
            final Contact contact1 = session.load(Contact.class, 2L);
            contact1.getPassword();
            System.out.println("----------------------------------------");
            final Contact contact2 = session.load(Contact.class, 2L);
            contact2.getPassword();
        }
    }

    @Test
    public void testLvl2Cache() {
        try (final Session session = getSessionFactory().openSession()){
            final Contact contact1 = session.load(Contact.class, 2L);
            contact1.getPassword();

        }

        System.out.println("----------------------------------------");


        try (final Session session = getSessionFactory().openSession()){
            final Contact contact = session.find(Contact.class, 2L);
            //contact.getPassword();
        }
        System.out.println("----------------------------------------");

        //session 1 update

        try (final Session session = getSessionFactory().openSession()){
            final Contact contact1 = session.find(Contact.class, 2L);
            //contact1.getPassword();
        }
    }


    @Test
    public void testQueryCache() {
        try (final Session session = getSessionFactory().openSession()){
            session.getTransaction().begin();
            final Query<Contact> query = session.createQuery("FROM Contact AS c", Contact.class);
            query.setCacheable(true);

            final List<Contact> list1 = query.list();

            System.out.println("----------------------------------------");
            final List<Contact> list2 = query.list();

            session.getTransaction().commit();
        }


        System.out.println("----------------------------------------");
        try (final Session session = getSessionFactory().openSession()){
            session.getTransaction().begin();
            final Query<Contact> query = session.createQuery("FROM Contact AS c", Contact.class);
            query.setHint(QueryHints.HINT_CACHEABLE, true);
            final List<Contact> list = query.list();


        }
    }

}
