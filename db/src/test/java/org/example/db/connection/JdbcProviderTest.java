package org.example.db.connection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.junit.Test;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;
import java.sql.Types;

public class JdbcProviderTest {

    private static final Logger LOGGER = LogManager.getLogger(JdbcProviderTest.class);
    private static final String SELECT_CONTACTS = "SELECT * FROM test_db.contact;";
    // .com/contacts/1     - 403 status
    // .com/contacts/5     - 200 status
    // .com/contacts/5 AND id > 0

    private static final String SELECT_CONTACT_BY_ID = "SELECT * FROM test_db.contact where id = {}";

    @Test
    public void testInjection() {
        LOGGER.warn(SELECT_CONTACT_BY_ID, 5);
        LOGGER.warn(SELECT_CONTACT_BY_ID, "5 AND id = 1");
        LOGGER.warn(SELECT_CONTACT_BY_ID, "5 left join Department on d.id = c.id");
    }

    @Test
    public void test() throws Exception {
        Connection connection = null;
        try {
             connection = JdbcProvider.getConnection();
        } finally {
            if (connection != null) {
                connection.close();
            }
        }
        Assert.assertNotNull(connection);

    }

    @Test
    public void testMetadata() throws Exception {
        try (final Connection connection = JdbcProvider.getConnection()){
            final DatabaseMetaData metaData = connection.getMetaData();
            LOGGER.info("URL: {}",metaData.getURL());
            LOGGER.info("username: {}",metaData.getUserName());
            LOGGER.info("Db: {}-{}", metaData.getDatabaseProductName(), metaData.getDatabaseProductVersion());
        }
    }

//      connection.close();
//      connection.createStatement();
//      connection.prepareStatement();
//      connection.prepareCall();
//      connection.setAutoCommit();
//      connection.setSavepoint();

//      statement.execute();
//      statement.executeUpdate();
//      statement.cancel();
//      statement.addBatch();
//      statement.executeBatch();

    @Test
    public void testStatement() throws Exception {
        try (final Connection connection = JdbcProvider.getConnection()){
            try (final Statement statement = connection.createStatement()) {
                try (final ResultSet resultSet = statement.executeQuery(SELECT_CONTACTS)) {
                    final ResultSetMetaData metaData = resultSet.getMetaData();
                    LOGGER.info("column count: {}",metaData.getColumnCount());
                    LOGGER.info("column name: {}", metaData.getColumnName(5));
                    LOGGER.info("column type (5): {}", metaData.getColumnType(5));
                    LOGGER.info("column type name (5): {}", metaData.getColumnTypeName(5));
                    LOGGER.info("column type size (5): {}", metaData.getColumnDisplaySize(5));

                    while (resultSet.next()) {
                        LOGGER.info("Contact: {} {}:{} -> {} {} : {}  {}",
                                resultSet.getInt(1),
                                resultSet.getString(2),
                                resultSet.getString(3),
                                resultSet.getString(4),
                                resultSet.getString(5),
                                resultSet.getDate("birthdate"),
                                resultSet.getString("country")
                                );
                    }
                }

                final int i = statement.executeUpdate("UPDATE test_db.department SET name = 'Engineering department' where id = 1");
                LOGGER.info("affected rows: {}", i);
            }
        }
    }


    @Test
    public void testPreparedStatement() throws Exception {
        try (final Connection connection = JdbcProvider.getConnection()) {
            try (final PreparedStatement preparedStatement = connection.prepareStatement("UPDATE department SET name = ? WHERE id = ?")){
                preparedStatement.setString(1, "QA Department");
                preparedStatement.setInt(2, 2);
                final int i = preparedStatement.executeUpdate();
                LOGGER.info("affected rows: {}", i);
            }
        }
    }

    @Test
    public void testCallableStatement() throws Exception {
        try (final Connection connection = JdbcProvider.getConnection()) {
            try (final CallableStatement statement = connection.prepareCall("{call getcontactlastnamebyid(?,?)}")){
                statement.setInt(1, 3);
                statement.registerOutParameter(2, Types.VARCHAR);
                statement.execute();

                final String string = statement.getString(2);
                LOGGER.info("result: {}", string);
            }
        }
    }
}
