package org.example.db.connection;

import org.example.model.BankCheck;
import org.example.model.BillingDetails;
import org.example.model.Contact;
import org.example.model.CreditCard;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.junit.Test;

import javax.persistence.EntityManager;
import java.time.LocalDate;

public class HibernateUtilTest {

    @Test
    public void test() {
        final EntityManager entityManager = HibernateUtil.getEntityManager();
        entityManager.getTransaction().begin();
        final Contact contact = new Contact("SuperAdmin", "pass", "Eric", "Cartman");
        entityManager.persist(contact);
        entityManager.getTransaction().commit();


        final Contact newContact = entityManager.find(Contact.class, 1L);

        entityManager.close();
        HibernateUtil.close();
    }

    @Test
    public void testSessionFactory() {
        final SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
        try (final Session session = sessionFactory.openSession()) {
            session.getTransaction().begin();
            final Contact contact = new Contact("SuperAdmin", "pass", "Eric", "Cartman");


            session.persist(contact);
            session.getTransaction().commit();
        }

        HibernateUtil.closeSF();
    }

    @Test
    public void initHibernate() {
        final SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
        try (final Session session = sessionFactory.openSession()) {
            session.getTransaction().begin();
            final Contact contact = new Contact("SuperAdmin", "pass", "Eric", "Cartman");
            session.persist(contact);
            final BillingDetails bankCheck = new BankCheck("Tony Stark", "Plaza", LocalDate.now(),
                    "Stark", 123456789L);
            session.persist(bankCheck);
            final BillingDetails creditCard = new CreditCard("Joe Black", "Home", LocalDate.now().minusDays(10),
                    123412341234L,LocalDate.now().plusYears(2).plusDays(3), "Black holder", 123);
            session.persist(creditCard);

            session.getTransaction().commit();
        }
        sessionFactory.close();
    }

    @Test
    public void initHibernate2() {
        HibernateUtil.getSessionFactory().close();
    }
}
