package org.example.db;

import org.example.db.connection.HibernateUtil;
import org.example.model.Contact;
import org.example.model.Course;
import org.example.model.CourseClass;
import org.example.model.Student;
import org.example.model.Tutor;
import org.example.model.UserRole;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.junit.AfterClass;
import org.junit.BeforeClass;

import java.util.ArrayList;
import java.util.List;

public abstract class AbstractHibernateTest {

    private static SessionFactory sessionFactory;

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    @BeforeClass
    public static void beforeClass() {
        sessionFactory = HibernateUtil.getSessionFactory();
        prepareDb();
    }

    private static void prepareDb() {
        try (final Session session = sessionFactory.openSession()) {
            final Transaction transaction = session.beginTransaction();

            final List<UserRole> userRoleList = new ArrayList<>();
            userRoleList.add(new UserRole("admin", true));
            userRoleList.add(new UserRole("user", false));

            final List<Contact> contactList = new ArrayList<>();
            contactList.add(new Contact("tonylogin", "12345", "Tony", "Stark"));
            contactList.add(new Contact("cartmanlogin", "qwerty", "Eric", "Cartman"));
            contactList.add(new Contact("gibsonlogin", "gfdgsdg", "Mel", "Gibson"));
            contactList.add(new Contact("johnylogin", "12fsadf345", "John", "Cena"));
            contactList.add(new Contact("bannerlogin", "123фывафы45", "Bruce", "Banner"));
            contactList.add(new Contact("userlogin", "testpass", "User", null));
            contactList.add(new Contact("adminlogin", "testpass", "Admin", null));
            for (int i = 0; i < contactList.size(); i++ ) {
                contactList.get(i).setUserRole(userRoleList.get(i % 2));
            }
            contactList.forEach(session::persist);

            final Tutor tutor = new Tutor("Joda Master");
            tutor.setContact(contactList.get(0));
            session.persist(tutor);

            final Student student = new Student();
            student.setContact(contactList.get(1));
            session.persist(student);

            final Course course = new Course("Java Programming - Beginners", "Java essentials and examples for beginner level programmers.");
            session.persist(course);

            final CourseClass courseClass = new CourseClass();
            courseClass.setCourse(course);
            courseClass.setLecturer(tutor);
            courseClass.setTrainer(tutor);
            courseClass.addStudent(student);
            session.persist(courseClass);

            transaction.commit();
        }
    }

    @AfterClass
    public static void afterClass() {
        sessionFactory.close();
    }

    public void printContact(Contact contact) {
        System.out.println(String.format("%s %s %s %s", contact.getFirstName(), contact.getLastName(), contact.getLogin(), contact.getPassword()));
    }

    public void printContact(UserRole role) {
        System.out.println(String.format("%s %s", role.getName(), role.getAdmin()));
    }
}
