package org.example.db;

import org.example.model.Contact;
import org.example.model.UserInfo;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.hibernate.transform.Transformers;
import org.junit.Ignore;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

public class JpqlHqlTest extends AbstractHibernateTest {

    @Test
    public void testFrom() {
        final Session session = getSessionFactory().openSession();
        final Query<Contact> from_contact = session.createQuery("from Contact", Contact.class);
        final List<Contact> resultList = from_contact.getResultList();
        resultList.forEach(contact ->
                System.out.println(String.format("%s %s %s %s", contact.getFirstName(), contact.getLastName(), contact.getLogin(), contact.getPassword())));
    }

    @Test
    public void testFromAS() {
        final Session session = getSessionFactory().openSession();
        final Query<Contact> from_contact = session.createQuery("from Contact AS c", Contact.class);
        final List<Contact> resultList = from_contact.getResultList();
        resultList.forEach(contact ->
                System.out.println(String.format("%s %s %s %s", contact.getFirstName(), contact.getLastName(), contact.getLogin(), contact.getPassword())));
    }

    @Test
    public void testSelectFromAS() {
        final Session session = getSessionFactory().openSession();
        final Query<String> from_contact = session.createQuery("select c.firstName from Contact AS c", String.class);
        final List<String> resultList = from_contact.getResultList();
        resultList.forEach(System.out::println);
    }

    @Test
    public void testSelectFromAsWithDiffObject() {
        final Session session = getSessionFactory().openSession();
        final Query<UserInfo> from_contact = session.createQuery("select c.firstName, c.lastName from Contact AS c")
                .setResultTransformer(Transformers.aliasToBean(UserInfo.class));
        final List<UserInfo> resultList = from_contact.getResultList();
        System.out.println();
        resultList.forEach(contact ->
                System.out.println(String.format("%s  ---  %s", contact.getFirstName(), contact.getLastName())));
    }

    @Test
    public void testSelectFromGroupBy() {
        final Session session = getSessionFactory().openSession();
        final Query<?> from_contact = session.createQuery(
                "select c.password, count(c.password)" +
                                "from Contact AS c " +
                                "where c.password not like '1%'" +
                                "group by c.password " +
                                "having count(c.password) > 0 " +
                                "order by count(c.password) desc" );
        final List<Object[]> resultList = (List<Object[]>) from_contact.getResultList();
        System.out.println();
        resultList.forEach(t ->
                System.out.println(String.format("%s  ---  %s", t[0], t[1])));
    }

    @Test
    public void testUpdate() {
        final Session session = getSessionFactory().openSession();
        final Transaction transaction = session.beginTransaction();
        final Query<?> from_contact = session
                .createQuery("update Contact set password = ?1 where lastName = :name") // 123
                .setParameter(1, "pass_changed")
                .setParameter("name", "Cartman");
        final int i = from_contact.executeUpdate();
        System.out.println(i);
        transaction.commit();
    }

    @Test
    public void testUpdateWithListParam() {
        final Session session = getSessionFactory().openSession();
        final Transaction transaction = session.beginTransaction();
        final Query<?> from_contact = session
                .createQuery("update Contact set password = ?1 where id in (?2)") // 123
                .setParameter(1, "pass_changed")
                .setParameter(2, Arrays.asList(7L, 8L, 9L, 22L, 23L, 24L));
        final int i = from_contact.executeUpdate();
        System.out.println(i);
        transaction.commit();
    }

    @Test
    public void testInsert() {
        final Session session = getSessionFactory().openSession();
        final Transaction transaction = session.beginTransaction();
        final Query<?> from_contact = session.createQuery(
                "insert into Contact(login, password, firstName, lastName) " +
                        "select concat(c2.login, '_test'), c2.password, c2.firstName, c2.lastName from Contact as c2");
        final int i = from_contact.executeUpdate();
        System.out.println(i);
        transaction.commit();
    }

    @Test
    public void testFromPagination() {
        int page = 2;
        int pageSize = 3;
        final Session session = getSessionFactory().openSession();
        final Query<Contact> from_contact = session.createQuery("from Contact", Contact.class)
                .setFirstResult((page - 1) * pageSize) // 3    (0, 1, 2, 3, 4, 5 ...)
                .setMaxResults(pageSize); // 3

        final List<Contact> resultList = from_contact.getResultList();
        resultList.forEach(contact ->
                System.out.println(String.format("%s %s %s %s", contact.getFirstName(), contact.getLastName(), contact.getLogin(), contact.getPassword())));
    }
}
