package org.example.db;

import org.example.db.connection.HibernateUtil;
import org.example.model.Course;
import org.hibernate.Session;
import org.junit.Test;

import javax.persistence.LockModeType;

public class LockTest extends AbstractHibernateTest {

    @Test
    public void test() throws Exception {
        try (final Session session = HibernateUtil.getSessionFactory().openSession()){
            session.getTransaction().begin();
            final Course course = session.find(Course.class, 1L, LockModeType.PESSIMISTIC_READ);
            course.setName("JAVA");
//            new Thread(() -> {
//                try (final Session session1 = HibernateUtil.getSessionFactory().openSession()){
//                    session1.getTransaction().begin();
//                    final Course course1 = session1.find(Course.class, 1L);
//                    course1.setDetails("JAVA _ 1");
//                    session1.getTransaction().commit();
//                }
//            }).start();
//            Thread.sleep(1000);
            session.getTransaction().commit();

        }
    }

}
