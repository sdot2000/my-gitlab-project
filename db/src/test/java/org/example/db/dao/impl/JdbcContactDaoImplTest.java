package org.example.db.dao.impl;

import org.example.db.dao.ContactDao;
import org.example.model.Contact;
import org.junit.Test;

import java.util.List;

public class JdbcContactDaoImplTest {


    @Test
    public void test() throws Exception{
        final ContactDao contactDao = JdbcContactDaoImpl.getInstance();

        final List<Contact> all = contactDao.getAll();
        final Contact contact = contactDao.get(6L);

        contactDao.create(new Contact("SuperAdmin", "pass", "Eric", "Cartman"));
        final List<Contact> all_2 = contactDao.getAll();

        final Contact superAdmin = contactDao.getByLogin("SuperAdmin");
        contactDao.delete(superAdmin.getId());

        final List<Contact> all_3 = contactDao.getAll();
        System.out.println();
    }
}
