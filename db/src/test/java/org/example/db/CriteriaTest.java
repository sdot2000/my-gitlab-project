package org.example.db;

import org.example.model.Contact;
import org.example.model.Student;
import org.example.model.UserRole;
import org.hibernate.Session;
import org.junit.Test;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.ParameterExpression;
import javax.persistence.criteria.Root;
import java.util.List;

public class CriteriaTest extends AbstractHibernateTest {

    @Test
    public void test() {
        try (final Session currentSession = getSessionFactory().openSession()){
            final CriteriaBuilder criteriaBuilder = currentSession.getCriteriaBuilder();

            final CriteriaQuery<Contact> query = criteriaBuilder.createQuery(Contact.class);
            final Root<Contact> root = query.from(Contact.class);

            query.select(root);

            final List<Contact> resultList = currentSession.createQuery(query).getResultList();

            resultList.forEach(this::printContact);
        }
    }


    @Test
    public void testWhereEquals() {
        try (final Session currentSession = getSessionFactory().openSession()){
            final CriteriaBuilder criteriaBuilder = currentSession.getCriteriaBuilder();

            final CriteriaQuery<Contact> query = criteriaBuilder.createQuery(Contact.class);
            final Root<Contact> root = query.from(Contact.class);

            query.select(root)
                    .where(criteriaBuilder.equal(root.get("password"), "12345"));
            //.le() -> <=
            //.lt() -> <
            //.ge() -> >=
            //.gt() -> >

            final List<Contact> resultList = currentSession.createQuery(query).getResultList();
            resultList.forEach(this::printContact);
        }
    }

    @Test
    public void testWhereLike() {
        try (final Session currentSession = getSessionFactory().openSession()){
            final CriteriaBuilder criteriaBuilder = currentSession.getCriteriaBuilder();

            final CriteriaQuery<Contact> query = criteriaBuilder.createQuery(Contact.class);
            final Root<Contact> root = query.from(Contact.class);

            query.select(root)
                    .where(criteriaBuilder.like(root.get("login"), "%ylogin"));

            final List<Contact> resultList = currentSession.createQuery(query).getResultList();
            resultList.forEach(this::printContact);
        }
    }

    @Test
    public void testWhereBetween() {
        try (final Session currentSession = getSessionFactory().openSession()){
            final CriteriaBuilder criteriaBuilder = currentSession.getCriteriaBuilder();

            final CriteriaQuery<Contact> query = criteriaBuilder.createQuery(Contact.class);
            final Root<Contact> root = query.from(Contact.class);

            query.select(root)
                    .where(criteriaBuilder.between(root.get("id"), 2, 4));

            final List<Contact> resultList = currentSession.createQuery(query).getResultList();
            resultList.forEach(this::printContact);
        }
    }

    @Test
    public void testWhereIsNull() {
        try (final Session currentSession = getSessionFactory().openSession()){
            final CriteriaBuilder criteriaBuilder = currentSession.getCriteriaBuilder();

            final CriteriaQuery<Contact> query = criteriaBuilder.createQuery(Contact.class);
            final Root<Contact> root = query.from(Contact.class);

            query.select(root)
                    .where(criteriaBuilder.isNull(root.get("lastName")));

            final List<Contact> resultList = currentSession.createQuery(query).getResultList();
            resultList.forEach(this::printContact);
        }
    }

    @Test
    public void testWhereIsTrue() {
        try (final Session currentSession = getSessionFactory().openSession()){
            final CriteriaBuilder criteriaBuilder = currentSession.getCriteriaBuilder();

            final CriteriaQuery<UserRole> query = criteriaBuilder.createQuery(UserRole.class);
            final Root<UserRole> root = query.from(UserRole.class);

            query.select(root)
                    .where(criteriaBuilder.isTrue(root.get("admin")));

            final List<UserRole> resultList = currentSession.createQuery(query).getResultList();
            resultList.forEach(this::printContact);
        }
    }

    @Test
    public void testWhereLikeOr() {
        try (final Session currentSession = getSessionFactory().openSession()){
            final CriteriaBuilder criteriaBuilder = currentSession.getCriteriaBuilder();

            final CriteriaQuery<Contact> query = criteriaBuilder.createQuery(Contact.class);
            final Root<Contact> root = query.from(Contact.class);

            query.select(root)
                    .where(criteriaBuilder.or(
                            criteriaBuilder.like(root.get("login"), "%ylogin"),
                            criteriaBuilder.like(root.get("login"), "%nlogin"))
                    )
                    .orderBy(criteriaBuilder.asc(root.get("userRole").get("admin")),
                            criteriaBuilder.asc(root.get("lastName")),
                            criteriaBuilder.asc(root.get("firstName")));

            final List<Contact> resultList = currentSession.createQuery(query)
                    .setMaxResults(3)
                    .getResultList();
            resultList.forEach(this::printContact);
        }
    }


    @Test
    public void testWithCount() {
        try (final Session currentSession = getSessionFactory().openSession()){
            final CriteriaBuilder criteriaBuilder = currentSession.getCriteriaBuilder();

            final CriteriaQuery<Long> query = criteriaBuilder.createQuery(Long.class);
            final Root<Contact> root = query.from(Contact.class);

            query.select(criteriaBuilder.count(root));
            //criteriaBuilder.countDistinct()
            //criteriaBuilder.sum()
            //criteriaBuilder.avg()
            //criteriaBuilder.max()
            //criteriaBuilder.min()

            final Long count = currentSession.createQuery(query).getSingleResult();
            System.out.println(count);
        }
    }


    @Test
    public void testFetch() {
        try (final Session currentSession = getSessionFactory().openSession()){
            final CriteriaBuilder criteriaBuilder = currentSession.getCriteriaBuilder();

            final CriteriaQuery<Contact> query = criteriaBuilder.createQuery(Contact.class);
            final Root<Contact> root = query.from(Contact.class);

            root.fetch("userRole");
            query.select(root);

            final List<Contact> resultList = currentSession.createQuery(query).getResultList();

            resultList.forEach(this::printContact);
        }
    }


    @Test
    public void testJoin() {
        try (final Session currentSession = getSessionFactory().openSession()){
            final CriteriaBuilder criteriaBuilder = currentSession.getCriteriaBuilder();

            final CriteriaQuery<Contact> query = criteriaBuilder.createQuery(Contact.class);
            final Root<Contact> root = query.from(Contact.class);
            root.join("userRole", JoinType.LEFT);
            query.select(root);

            final List<Contact> resultList = currentSession.createQuery(query).getResultList();

            resultList.forEach(this::printContact);
        }
    }

    @Test
    public void testJoinWithParameter() {
        try (final Session currentSession = getSessionFactory().openSession()){
            final CriteriaBuilder criteriaBuilder = currentSession.getCriteriaBuilder();

            final CriteriaQuery<Contact> query = criteriaBuilder.createQuery(Contact.class);
            final Root<Contact> root = query.from(Contact.class);
            final Join<Object, Object> userRoleJoin = root.join("userRole", JoinType.LEFT);
            query.select(root);

            final ParameterExpression<Boolean> isAdmin = criteriaBuilder.parameter(Boolean.class);
            query.where(criteriaBuilder.equal(userRoleJoin.get("admin"), isAdmin));

            final List<Contact> resultList = currentSession.createQuery(query)
                    .setParameter(isAdmin, true)
                    .getResultList();

            resultList.forEach(this::printContact);
        }
    }

    @Test
    public void test2() {
        try (final Session currentSession = getSessionFactory().openSession()){
            final CriteriaBuilder criteriaBuilder = currentSession.getCriteriaBuilder();

            final CriteriaQuery<Student> query = criteriaBuilder.createQuery(Student.class);
            final Root<Student> root = query.from(Student.class);
            query.select(root);

            final List<Student> resultList = currentSession.createQuery(query)
                    .getResultList();

            resultList.size();
        }
    }
}
