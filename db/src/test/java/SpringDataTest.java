import org.example.db.repository.AdvancedContactRepository;
import org.example.db.repository.ContactRepository;
import org.example.model.Contact;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("/spring-context.xml")
public class SpringDataTest {

    @Autowired
    private ContactRepository contactRepository;

    @Autowired
    private AdvancedContactRepository advancedContactRepository;

//    public SpringDataTest(ContactRepository contactRepository) {
//        this.contactRepository = contactRepository;
//    }

    @Test
    public void test() {
        final long count = contactRepository.count();
        final List<Contact> all = contactRepository.findAll();
        final Contact contact = new Contact("spring-developer2", "spring2", "JavaStudent", "Junior");
        final Contact save = contactRepository.save(contact);

        System.out.println(count);
    }

    @Test
    public void test2() {
        final Optional<Contact> user = contactRepository.findByFirstName("User");
        System.out.println();
        //final Stream<Contact> login = contactRepository.streamByLoginEndsWith("login");

        final List<Contact> login = contactRepository.findAllByLoginEndsWith("nlogin");
        System.out.println();

        final List<Contact> adminContacts = contactRepository.getAdminContacts();
        System.out.println();

        final Iterable all = advancedContactRepository
                .findAll(Sort.by(Sort.Direction.DESC, "login", "firstName"));
        System.out.println();
        final Sort sortBy = Sort.by(Sort.Order.asc("login"), Sort.Order.asc("firstName"));
        final Iterable all1 = advancedContactRepository
                .findAll(sortBy);
        System.out.println();

        final Page<Contact> all2 = advancedContactRepository.findAll(PageRequest.of(1, 5, sortBy));
        System.out.println();
    }
}
