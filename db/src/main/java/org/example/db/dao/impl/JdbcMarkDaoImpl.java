package org.example.db.dao.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.example.db.connection.JdbcProvider;
import org.example.db.dao.MarkDao;
import org.example.model.Mark;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class JdbcMarkDaoImpl implements MarkDao {

    private static final Logger LOGGER = LogManager.getLogger(JdbcMarkDaoImpl.class);

    private static MarkDao markDao;

    private JdbcMarkDaoImpl() { }

    public static MarkDao getInstance() throws Exception {
        if (markDao == null) {
            synchronized (JdbcMarkDaoImpl.class) {
                if (markDao == null) {
                    markDao = new JdbcMarkDaoImpl();
                }
            }
        }

        return markDao;
    }

    @Override
    public Mark getByLecturerLogin(String login) throws Exception {
        try (Connection c = JdbcProvider.getConnection()) {
            try (PreparedStatement ps = c.prepareStatement("SELECT id, " +
                    "mark, course_class_id, student_id FROM local_db.course_class_student " +
                    "JOIN local_db.course_class c1 ON course_class_id = c1.id " +
                    "JOIN local_db.lecturer c2 ON c1.lecturer_id = c2.id " +
                    "JOIN local_db.tutor c3 ON c2.tutor_id = c3.id " +
                    "JOIN local_db.contact c4 ON c3.contact_id = c4.id " +
                    "WHERE c4.login = ?")) {
                ps.setString(1, login);
                try (ResultSet rs = ps.executeQuery()){
                    if (rs.next()) {
                        return fillMark(rs);
                    } else {
                        return null;
                    }
                }
            }
        } catch (Exception e) {
            LOGGER.error("Error during get mark by lecturer login", e);
            throw new Exception("Error during get mark by lecturer login", e);
        }
    }

    @Override
    public Mark getByTrainerLogin(String login) throws Exception {
        try (Connection c = JdbcProvider.getConnection()) {
            try (PreparedStatement ps = c.prepareStatement("SELECT id, " +
                    "mark, course_class_id, student_id FROM local_db.course_class_student " +
                    "JOIN local_db.course_class c1 ON course_class_id = c1.id " +
                    "JOIN local_db.trainer c2 ON c1.trainer_id = c2.id " +
                    "JOIN local_db.tutor c3 ON c2.tutor_id = c3.id " +
                    "JOIN local_db.contact c4 ON c3.contact_id = c4.id " +
                    "WHERE c4.login = ?")) {
                ps.setString(1, login);
                try (ResultSet rs = ps.executeQuery()){
                    if (rs.next()) {
                        return fillMark(rs);
                    } else {
                        return null;
                    }
                }
            }
        } catch (Exception e) {
            LOGGER.error("Error during get mark by trainer login", e);
            throw new Exception("Error during get mark by trainer login", e);
        }
    }

    @Override
    public Mark getByStudentLogin(String login) throws Exception {
        try (Connection c = JdbcProvider.getConnection()) {
            try (PreparedStatement ps = c.prepareStatement("SELECT id, " +
                    "mark, course_class_id, student_id FROM local_db.course_class_student " +
                    "JOIN local_db.student c1 ON student_id = c1.id " +
                    "JOIN local_db.contact c2 ON c1.contact_id = c2.id " +
                    "WHERE c2.login = ?")) {
                ps.setString(1, login);
                try (ResultSet rs = ps.executeQuery()){
                    if (rs.next()) {
                        return fillMark(rs);
                    } else {
                        return null;
                    }
                }
            }
        } catch (Exception e) {
            LOGGER.error("Error during get mark by student login", e);
            throw new Exception("Error during get mark by student login", e);
        }
    }

    @Override
    public Mark get(Long id) throws Exception {
        try (Connection c = JdbcProvider.getConnection()) {
            try (PreparedStatement ps = c.prepareStatement("SELECT * FROM local_db.course_class_student WHERE id = ?")) {
                ps.setLong(1, id);
                try (ResultSet rs = ps.executeQuery()){
                    if (rs.next()) {
                        return fillMark(rs);
                    } else {
                        return null;
                    }
                }
            }
        } catch (Exception e) {
            LOGGER.error("Error during get mark by id", e);
            throw new Exception("Error during get mark by id", e);
        }
    }

    @Override
    public List<Mark> getAll() throws Exception {
        try (Connection c = JdbcProvider.getConnection()) {
            try (Statement s = c.createStatement()) {
                try (ResultSet rs = s.executeQuery("SELECT * FROM local_db.course_class_student")){
                    List<Mark> markList = new ArrayList<>();
                    while (rs.next()) {
                        final Mark mark = fillMark(rs);
                        markList.add(mark);
                    }
                    return markList;
                }
            }
        } catch (Exception e) {
            LOGGER.error("Error during get all marks", e);
            throw new Exception("Error during get all marks", e);
        }
    }

    @Override
    public void create(Mark item) throws Exception {
        try (Connection c = JdbcProvider.getConnection()) {
            try (PreparedStatement ps = c.prepareStatement(
                    "INSERT INTO local_db.course_class_student (mark, course_class_id, student_id) " +
                            "VALUES (?, ?, ?)")) {
                fillMarkQuery(item, ps);
                ps.executeUpdate();
            }
        } catch (Exception e) {
            LOGGER.error("Error during create mark", e);
            throw new Exception("Error during create mark", e);
        }
    }

    @Override
    public void update(Mark item) throws Exception {
        try (Connection c = JdbcProvider.getConnection()) {
            try (PreparedStatement ps = c.prepareStatement(
                    "UPDATE local_db.course_class_student SET " +
                            "mark = ?, course_class_id = ?, student_id = ? " +
                            "WHERE id = ?")) {
                fillMarkQuery(item, ps);
                ps.setLong(4, item.getId());
                ps.executeUpdate();
            }
        } catch (Exception e) {
            String msg = "Error during update mark";
            LOGGER.error(msg, e);
            throw new Exception(msg, e);
        }
    }

    @Override
    public void delete(Long id) throws Exception {
        try (Connection c = JdbcProvider.getConnection()) {
            try (PreparedStatement ps = c.prepareStatement("DELETE FROM local_db.course_class_student WHERE id = ?")) {
                ps.setLong(1, id);
                final int i = ps.executeUpdate(); // 1 or 0;
                if (i == 0) {
                    throw new Exception("No mark for id=" + id);
                }
            }
        } catch (Exception e) {
            String msg = "Error during delete mark";
            LOGGER.error(msg, e);
            throw new Exception(msg, e);
        }
    }

    @Override
    public Mark getByName(String name) {
        throw new IllegalStateException("Method not supported");
    }

    private Mark fillMark(ResultSet rs) throws SQLException {
        final Mark mark = new Mark();
        mark.setId(rs.getLong("id"));
        mark.setMark(rs.getInt("mark"));
        mark.setCourseClassId(rs.getLong("course_class_id"));
        mark.setStudentId(rs.getLong("student_id"));
        return mark;
    }

    private void fillMarkQuery(Mark mark, PreparedStatement ps) throws SQLException {
        ps.setInt(1, mark.getMark());
        ps.setLong(2, mark.getCourseClassId());
        ps.setLong(3, mark.getStudentId());
    }
}
