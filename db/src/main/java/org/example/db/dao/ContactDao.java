package org.example.db.dao;

import org.example.model.Contact;

public interface ContactDao extends CommonDao<Contact> {

    Contact getByLogin(String login) throws Exception;

}
