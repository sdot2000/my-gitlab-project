package org.example.db.dao.impl;

import org.example.db.dao.ContactDao;
import org.example.db.repository.ContactRepository;
import org.example.model.Contact;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SpringContactDaoImpl implements ContactDao {

    @Autowired
    private ContactRepository contactRepository;

    @Override
    public Contact getByLogin(String login) throws Exception {
        return null;
    }

    @Override
    public Contact get(Long id) throws Exception {
        return contactRepository.findById(id).orElse(null);
    }

    @Override
    public List<Contact> getAll() throws Exception {
        return null;
    }

    @Override
    public void create(Contact item) throws Exception {

    }

    @Override
    public void update(Contact item) throws Exception {

    }

    @Override
    public void delete(Long id) throws Exception {

    }

    @Override
    public Contact getByName(String name) {
        return null;
    }
}
