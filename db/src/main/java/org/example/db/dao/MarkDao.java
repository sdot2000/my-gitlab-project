package org.example.db.dao;

import org.example.model.Mark;

public interface MarkDao extends CommonDao<Mark> {

    Mark getByLecturerLogin(String login) throws Exception;
    Mark getByTrainerLogin(String login) throws Exception;
    Mark getByStudentLogin(String login) throws Exception;

}
