package org.example.db.dao.impl;

import java.util.List;
import org.example.db.connection.HibernateUtil;
import org.example.db.dao.ContactDao;
import org.example.model.Contact;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.NativeQuery;
import org.hibernate.query.Query;

public class HibernateContactDaoImpl implements ContactDao {

    private SessionFactory sessionFactory = HibernateUtil.getSessionFactory();

    @Override
    public Contact getByLogin(String login) {
        try (final Session session = sessionFactory.openSession()){
            final Query<Contact> query = session.createQuery("SELECT c FROM Contact c WHERE c.login = :login", Contact.class);
            query.setParameter("login", login);
            return query.getSingleResult();
        }
    }

    @Override
    public Contact get(Long id) {
        try (final Session session = sessionFactory.openSession()){
            return session.get(Contact.class, id);
        }
    }

    @Override
    public List<Contact> getAll() {
        try (final Session session = sessionFactory.openSession()){
            final NativeQuery<Contact> nativeQuery = session.createNativeQuery("SELECT * FROM contact;", Contact.class);
            return nativeQuery.getResultList();
        }
    }

    @Override
    public void create(Contact contact) {
        try (final Session session = sessionFactory.openSession()){
            session.getTransaction().begin();
            session.save(contact);
            session.getTransaction().commit();
        }
    }

    @Override
    public void update(Contact contact) {
        try (final Session session = sessionFactory.openSession()){
            session.getTransaction().begin();
            session.update(contact);
            session.getTransaction().commit();
        }
    }

    @Override
    public void delete(Long id) {
        try (final Session session = sessionFactory.openSession()){
            session.getTransaction().begin();
            final Contact contact = session.get(Contact.class, id);
            session.delete(contact);
            session.getTransaction().commit();
        }
    }

    @Override
    public Contact getByName(String name) {
        throw new IllegalStateException("Method not supported.");
    }
}
