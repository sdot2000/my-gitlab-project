package org.example.db.dao.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.example.db.connection.JdbcProvider;
import org.example.db.dao.ContactDao;
import org.example.model.Contact;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class JdbcContactDaoImpl implements ContactDao {

    private static final Logger LOGGER = LogManager.getLogger(JdbcContactDaoImpl.class);

    private static ContactDao contactDao;

    private JdbcContactDaoImpl() { }

    public static ContactDao getInstance() throws Exception {
        if (contactDao == null) {
            synchronized (JdbcContactDaoImpl.class) {
                if (contactDao == null) {
                    contactDao = new JdbcContactDaoImpl();
                }
            }
        }

        return contactDao;
    }

    @Override
    public Contact getByLogin(String login) throws Exception {
        try (Connection c = JdbcProvider.getConnection()) {
            try (PreparedStatement ps = c.prepareStatement("SELECT * FROM local_db.contact WHERE login = ?")) {
                ps.setString(1, login);
                try (ResultSet rs = ps.executeQuery()){
                    if (rs.next()) {
                        return fillContact(rs);
                    } else {
                        return null;
                    }
                }
            }
        } catch (Exception e) {
            LOGGER.error("Error during get contact by login", e);
            throw new Exception("Error during get contact by login", e);
        }
    }

    @Override
    public Contact get(Long id) throws Exception {
        try (Connection c = JdbcProvider.getConnection()) {
            try (PreparedStatement ps = c.prepareStatement("SELECT * FROM local_db.contact WHERE id = ?")) {
                ps.setLong(1, id);
                try (ResultSet rs = ps.executeQuery()){
                    if (rs.next()) {
                        return fillContact(rs);
                    } else {
                        return null;
                    }
                }
            }
        } catch (Exception e) {
            LOGGER.error("Error during get contact by id", e);
            throw new Exception("Error during get contact by id", e);
        }
    }

    @Override
    public List<Contact> getAll() throws Exception {
        try (Connection c = JdbcProvider.getConnection()) {
            try (Statement s = c.createStatement()) {
                try (ResultSet rs = s.executeQuery("SELECT * FROM local_db.contact")){
                    List<Contact> contactList = new ArrayList<>();
                    while (rs.next()) {
                        final Contact contact = fillContact(rs);
                        contactList.add(contact);
                    }
                    return contactList;
                }
            }
        } catch (Exception e) {
            LOGGER.error("Error during get all contacts", e);
            throw new Exception("Error during get all contacts", e);
        }
    }

    @Override
    public void create(Contact item) throws Exception {
        try (Connection c = JdbcProvider.getConnection()) {
            try (PreparedStatement ps = c.prepareStatement(
                    "INSERT INTO local_db.contact (login, password, first_name, last_name, user_role_id) " +
                            "VALUES (?, ?, ?, ?, ?)")) {
                fillContactQuery(item, ps);
                ps.executeUpdate();
            }
        } catch (Exception e) {
            LOGGER.error("Error during create contact", e);
            throw new Exception("Error during create contact", e);
        }
    }

    @Override
    public void update(Contact item) throws Exception {
        try (Connection c = JdbcProvider.getConnection()) {
            try (PreparedStatement ps = c.prepareStatement(
                    "UPDATE local_db.contact SET " +
                            "login = ?, password = ?, first_name = ?, last_name = ?, user_role_id = ? " +
                            "WHERE id = ?")) {
                fillContactQuery(item, ps);
                ps.setLong(6, item.getId());
                ps.executeUpdate();
            }
        } catch (Exception e) {
            String msg = "Error during update contact";
            LOGGER.error(msg, e);
            throw new Exception(msg, e);
        }
    }

    @Override
    public void delete(Long id) throws Exception {
        try (Connection c = JdbcProvider.getConnection()) {
            try (PreparedStatement ps = c.prepareStatement("DELETE FROM local_db.contact WHERE id = ?")) {
                ps.setLong(1, id);
                final int i = ps.executeUpdate(); // 1 or 0;
                if (i == 0) {
                    throw new Exception("No contact for id=" + id);
                }
            }
        } catch (Exception e) {
            String msg = "Error during delete contact";
            LOGGER.error(msg, e);
            throw new Exception(msg, e);
        }
    }

    @Override
    public Contact getByName(String name) {
        throw new IllegalStateException("Method not supported");
    }

    private Contact fillContact(ResultSet rs) throws SQLException {
        final Contact contact = new Contact();
        contact.setId(rs.getLong("id"));
        contact.setLogin(rs.getString("login"));
        contact.setPassword(rs.getString("password"));
        contact.setFirstName(rs.getString("firstName"));
        contact.setLastName(rs.getString("lastName"));
        contact.setUserRole(rs.getInt("userRole"));
        return contact;
    }

    private void fillContactQuery(Contact contact, PreparedStatement ps) throws SQLException {
        ps.setString(1, contact.getLogin());
        ps.setString(2, contact.getPassword());
        ps.setString(3, contact.getFirstName());
        ps.setString(4, contact.getLastName());
        ps.setInt(5, contact.getUserRole());
    }
}
