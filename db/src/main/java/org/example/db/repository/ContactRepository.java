package org.example.db.repository;

import org.example.model.Contact;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

@Repository
public interface ContactRepository extends CrudRepository<Contact, Long> {

    @Override
    List<Contact> findAll();

    Optional<Contact> findByFirstName(String firstName);

    @Transactional
    List<Contact> findAllByLoginEndsWith(String login);

    @Query("SELECT c FROM Contact c where c.userRole is not null")
    List<Contact> getAdminContacts();
}
