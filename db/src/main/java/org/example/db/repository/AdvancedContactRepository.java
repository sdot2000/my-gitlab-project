package org.example.db.repository;

import org.example.model.Contact;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface AdvancedContactRepository extends PagingAndSortingRepository<Contact, Long> {
}
