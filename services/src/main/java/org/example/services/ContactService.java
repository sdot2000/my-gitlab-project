package org.example.services;

import org.example.model.Contact;

import java.util.List;

public interface ContactService {

    List<Contact> getContacts();

    void addContact(Contact contact);

    void deleteContact(Long id);
}
