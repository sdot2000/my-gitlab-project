package org.example.services.impl;

import org.example.model.Contact;
import org.example.services.ContactService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
public class FakeContactServiceImpl implements ContactService {

    private static Long ID = 5L;
    private static List<Contact> defaultContacts = new ArrayList<Contact>() {{
        add(new Contact(1L,"admin", "admin", "Ivan", "Bold"));
        add(new Contact(2L,"student", "student", "Vasili", null));
        add(new Contact(3L,"worker", "worker", "Tony", "Stark"));
        add(new Contact(4L,"superAdmin", "sadmin", "Hulk", "Hogan"));

        add(new Contact(5L,"22admin", "admin", "Ivan", "Bold"));
        add(new Contact(6L,"22student", "student", "Vasili", null));
        add(new Contact(7L,"22worker", "worker", "Tony", "Stark"));
        add(new Contact(8L,"22superAdmin", "sadmin", "Hulk", "Hogan"));

        add(new Contact(9L,"33admin", "admin", "Ivan", "Bold"));
        add(new Contact(10L,"33student", "student", "Vasili", null));
        add(new Contact(11L,"33worker", "worker", "Tony", "Stark"));
        add(new Contact(12L,"33superAdmin", "sadmin", "Hulk", "Hogan"));

        add(new Contact(13L,"44admin", "admin", "Ivan", "Bold"));
        add(new Contact(14L,"44student", "student", "Vasili", null));
        add(new Contact(15L,"44worker", "worker", "Tony", "Stark"));
        add(new Contact(16L,"44superAdmin", "sadmin", "Hulk", "Hogan"));
    }};

    @Override
    public List<Contact> getContacts() {
        return defaultContacts;
    }

    @Override
    public void addContact(Contact contact) {
        if (contact.getId() == null) {
            contact.setId(ID++);
            defaultContacts.add(contact);
        } else {
            deleteContact(contact.getId());
            defaultContacts.add(contact);
        }
    }

    @Override
    public void deleteContact(Long id) {
        defaultContacts.stream()
                .filter(contact -> contact.getId().equals(id))
                .findAny()
                .ifPresent(defaultContacts::remove);
    }


    public static void main(String[] args) {
        final ArrayList<Integer> objects = new ArrayList<>();
        objects.add(1);
        objects.add(2);
        objects.add(3);
        objects.add(4);
        objects.add(5);
        objects.add(6);

        for (Integer integer : objects) {
            if (integer > 3) {
                System.out.println("-");
                continue;
            }
            System.out.println(integer);
        }
    }
}
