package org.example.services.impl;

import org.example.model.Contact;
import org.example.services.AuthorizationService;

public class FakeAuthorizationServiceImpl implements AuthorizationService {

    private static AuthorizationService instance;

    public static AuthorizationService getInstance() {
        if (instance == null) {
            synchronized (AuthorizationService.class) {
                if (instance == null) {
                    instance = new FakeAuthorizationServiceImpl();
                }
            }
        }
        return instance;
    }

    private FakeAuthorizationServiceImpl() {

    }

    @Override
    public Contact authorize(String login, String password) {
        if ("admin".equals(login) && "admin".equals(password)) {
            final Contact contact = new Contact("admin", "admin", "Ivan", "Bold");
            contact.setId(-1L);
            return contact;
        }
        else if ("tutor".equals(login) && "tutor".equals(password)) {
            final Contact contact = new Contact("tutor", "tutor", "Gregor", "Minchev");
            contact.setId(-2L);
            return contact;
        }
        else if ("student".equals(login) && "student".equals(password)) {
            final Contact contact = new Contact("student", "student", "Vasili", "Terkin");
            contact.setId(-3L);
            return contact;
        }
        else {
            return null;
        }
    }
}
