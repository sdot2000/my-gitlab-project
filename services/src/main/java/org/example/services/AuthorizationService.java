package org.example.services;

import org.example.model.Contact;

public interface AuthorizationService {

    Contact authorize(String login, String password);

}
