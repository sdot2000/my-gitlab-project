package org.example.rest;

import org.example.db.repository.ContactRepository;
import org.example.model.Contact;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/contacts")
public class ContactsController {

    @Autowired
    private ContactRepository contactRepository;

    @GetMapping(path = "", produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    public ResponseEntity<List<Contact>> read() {
        final List<Contact> contacts = contactRepository.findAll();
        if (contacts.isEmpty()) {
            //return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            return ResponseEntity.noContent().build();
        } else {
            //new ResponseEntity<>(contacts, HttpStatus.OK);
            return ResponseEntity.ok().body(contacts);
        }
    }

    @GetMapping(path = "{id}")
    public ResponseEntity<Contact> get(@PathVariable("id") Long id) {
        final Contact contact = contactRepository.findById(id).orElse(null);
        if (contact == null) {
            return ResponseEntity.notFound().build();
        } else {
            return ResponseEntity.ok().body(contact);
        }
    }

    @PostMapping
    public ResponseEntity<?> create(@RequestBody Contact contact) {
        try {
            contactRepository.save(contact);
            return ResponseEntity.ok().body(contact);
        } catch (Throwable th) {
            return ResponseEntity.badRequest().body(th.getLocalizedMessage());
        }
    }

    @PutMapping(path = "{id}")
    public ResponseEntity<Contact> update(@PathVariable("id") Long id, @RequestBody Contact contact) {
        if (id.equals(contact.getId())) {
            contactRepository.save(contact);
            return ResponseEntity.ok().body(contact);
        } else {
            return ResponseEntity.badRequest().build();
        }
    }

    @DeleteMapping(path = "{id}")
    public ResponseEntity<?> delete(@PathVariable("id") Long id) {
        try {
            contactRepository.deleteById(id);
            return ResponseEntity.noContent().build();
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(e.getLocalizedMessage());
        }
    }
}
