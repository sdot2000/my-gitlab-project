package org.example.web.util;

public interface WebConstants {

    String REDIRECT_TO_CONTACTS_URL = "/contacts.jsp";
    String REDIRECT_TO_MARKS_URL = "/marks.jsp";

    String USER_ID_PARAM = "userId";
}
