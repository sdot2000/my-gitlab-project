package org.example.web.filter;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Arrays;
import java.util.function.Predicate;
import java.util.stream.Stream;

import static org.example.web.util.WebConstants.USER_ID_PARAM;

@WebFilter(
        filterName = "AuthFilter",
        urlPatterns = "/*",
        initParams = {
                @WebInitParam(name = AuthenticationFilter.IS_ACTIVE_FILTER_PARAM,
                        value = "false",
                        description = "activation of this filter"
                )
        }
        )
public class AuthenticationFilter implements Filter {

    public static final String IS_ACTIVE_FILTER_PARAM = "isActive";

    private boolean active;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        final String isActiveString = filterConfig.getInitParameter(IS_ACTIVE_FILTER_PARAM);
        active = isActiveString == null || isActiveString.toLowerCase().equals("true");
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        final HttpServletRequest req = (HttpServletRequest) request;
        final HttpServletResponse res = (HttpServletResponse) response;

        if(!active || hasValidSession(req) || bypassFilter(req)) {
            chain.doFilter(request, res);
        } else {
            res.sendError(HttpServletResponse.SC_UNAUTHORIZED);
        }
    }

    @Override
    public void destroy() {
        //do nothing
    }

    private boolean hasValidSession(HttpServletRequest request) {
        final HttpSession session = request.getSession();
        return session != null && session.getAttribute(USER_ID_PARAM) != null;
    }

    private boolean bypassFilter(HttpServletRequest request) {
        final String contextPath = request.getContextPath();
        final String path = request.getRequestURI().replaceFirst(contextPath, "");
        return Stream.of("/login.jsp", "/auth")
                .anyMatch(path::equalsIgnoreCase);

//              java 7 style
//              .anyMatch(new Predicate<String>() {
//                    @Override
//                    public boolean test(String s) {
//                        return path.equalsIgnoreCase(s);
//                    }
//                });
    }
}
