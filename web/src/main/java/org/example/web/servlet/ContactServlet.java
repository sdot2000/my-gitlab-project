package org.example.web.servlet;

import org.example.model.Contact;
import org.example.services.ContactService;
import org.example.services.impl.FakeContactServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

import static org.example.web.util.WebConstants.REDIRECT_TO_CONTACTS_URL;

@WebServlet(urlPatterns = "/contacts", name = "ContactServlet")
public class ContactServlet extends HttpServlet {

    private final ContactService contactService = new FakeContactServiceImpl();

    private static final String CONTACTS_ATTR = "contacts";

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        final List<Contact> contacts = contactService.getContacts();
        req.setAttribute(CONTACTS_ATTR, contacts);

        req.getServletContext().getRequestDispatcher(REDIRECT_TO_CONTACTS_URL).forward(req, resp); //todo include() diff?
    }
}
