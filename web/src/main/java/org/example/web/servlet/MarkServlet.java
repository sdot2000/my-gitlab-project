package org.example.web.servlet;

import org.example.model.Mark;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

import static org.example.web.util.WebConstants.REDIRECT_TO_MARKS_URL;

@WebServlet(urlPatterns = "/marks", name = "MarkServlet")
public class MarkServlet extends HttpServlet {

    private final MarkService markService = new FakeMarkServiceImpl();

    private static final String MARKS_ATTR = "marks";

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        final List<Mark> marks = markService.getMarks();
        req.setAttribute(MARKS_ATTR, marks);

        req.getServletContext().getRequestDispatcher(REDIRECT_TO_MARKS_URL).forward(req, resp); //todo include() diff?
    }
}
