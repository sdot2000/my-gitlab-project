package org.example.web.servlet;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class MainWithParamsServlet extends HttpServlet {

    private Integer maxUsersCount;
    private String appName;
    private String currencyURL;

    private ExecutorService executorService;

    @Override
    public void init() {
        maxUsersCount = Integer.valueOf(getServletConfig().getInitParameter("maxUsersCount"));
        appName = getServletContext().getInitParameter("appName");
        currencyURL = getServletConfig().getInitParameter("currencyURL");
        executorService = Executors.newSingleThreadExecutor();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html");
        final PrintWriter writer = resp.getWriter();
        writer.write("<h1>Main page with params</h1>");
        writer.write(String.format("<p>maxUsersCount = %s</p>", maxUsersCount));
        writer.write(String.format("<p>currencyURL = %s</p>", currencyURL));
        writer.write(String.format("<p>appName = %s</p>", appName));
    }

    @Override
    public void destroy() {
        executorService.shutdown();
    }
}
