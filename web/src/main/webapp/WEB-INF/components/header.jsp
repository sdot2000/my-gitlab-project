<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<header>
    <H1>Web Application</H1>
    <c:if test="${param.disableMenu != 'true'}">
        <div>
            <a href="index.jsp">Home</a>
            <c:if test="${requestScope.admin == 'true'}">
            <a href="index.jsp">Settings</a>
            </c:if>
            <a href="index.jsp">Logout</a>
        </div>
    </c:if>
</header>
