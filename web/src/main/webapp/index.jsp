<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ page import="java.time.LocalDateTime"%>
<%@ page import="org.example.web.util.WebConstants" %>

<html>
<body>
<%@ include file="WEB-INF/components/header.jsp"%>
<h2>Hello World!</h2>
<%! LocalDateTime currentTime = LocalDateTime.now();%>
<%! double random = Math.random();%>
<%! Object d = LocalDateTime.now(); Object f = LocalDateTime.now();%>

<p>Current time: <%=currentTime%></p>
<p>+3 days: <%=currentTime.plusDays(3)%></p>
<p>Random number: <%=random%></p>

<p>User id: <%=session.getAttribute(WebConstants.USER_ID_PARAM)%></p>
<p>Host: <%=request.getRemoteHost()%></p>
<%@ include file="WEB-INF/components/footer.jsp"%>
<%--<jsp:include page="WEB-INF/components/footer.jsp"/>--%>

<jsp:useBean id="profile" class="org.example.model.Contact">
    <jsp:setProperty name="profile" property="login" value="beanLogin"/>
    <jsp:setProperty name="profile" property="password" value="beanPassword"/>
</jsp:useBean>
<jsp:setProperty name="profile" property="firstName" value="beanBob"/>
<jsp:setProperty name="profile" property="lastName" value="beanBlack"/>

<ul>
    <li>login: <jsp:getProperty name="profile" property="login"/></li>
    <li>password: <jsp:getProperty name="profile" property="password"/></li>
    <li>firstName: <jsp:getProperty name="profile" property="firstName"/></li>
    <li>lastName: <jsp:getProperty name="profile" property="lastName"/></li>
</ul>


</body>
</html>
