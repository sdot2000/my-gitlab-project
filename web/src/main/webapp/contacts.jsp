<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Contacts</title>
</head>
<body>
<jsp:include page="WEB-INF/components/header.jsp"/>
<h3>Contact List:</h3>
<table>
    <tr>
        <th>login</th>
        <th>password</th>
        <th>firstName</th>
        <th>lastName</th>
    </tr>
    <c:forEach items="${requestScope.contacts}" var="contact">
        <tr>
            <td><c:out value="${contact.login}" default="--"/></td>
            <td><c:out value="${contact.password}" default="--"/></td>
            <td><c:out value="${contact.firstName}" default="--"/></td>
            <td><c:out value="${contact.lastName}" default="--"/></td>
        </tr>
    </c:forEach>
</table>

<jsp:include page="WEB-INF/components/footer.jsp"/>
</body>
</html>
