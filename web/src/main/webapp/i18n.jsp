<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="ru_RU"/> <%-- value=${sessionScope.locale} --%>
<fmt:setBundle basename="props" var="msg"/>
<html>
<head>
    <title>Profile</title>
</head>
<body>


<h2><fmt:message key="profile.title" bundle="${msg}"/></h2>
<ul>
    <li><fmt:message key="profile.firstname" bundle="${msg}"/></li>
    <li><fmt:message key="profile.lastname" bundle="${msg}"/></li>
    <li><fmt:message key="profile.birthdate" bundle="${msg}"/></li>
    <li><fmt:message key="profile.country" bundle="${msg}"/></li>
</ul>
</body>
</html>
