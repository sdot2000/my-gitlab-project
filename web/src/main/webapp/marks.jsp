<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Marks</title>
</head>
<body>
<jsp:include page="WEB-INF/components/header.jsp"/>
<h3>Mark List:</h3>
<table>
    <tr>
        <th>mark</th>
        <th>courseClassId</th>
        <th>studentId</th>
    </tr>
    <c:forEach items="${requestScope.marks}" var="mark">
        <tr>
            <td><c:out value="${mark.mark}" default="--"/></td>
            <td><c:out value="${mark.courseClassId}" default="--"/></td>
            <td><c:out value="${mark.studentId}" default="--"/></td>
        </tr>
    </c:forEach>
</table>

<jsp:include page="WEB-INF/components/footer.jsp"/>
</body>
</html>
