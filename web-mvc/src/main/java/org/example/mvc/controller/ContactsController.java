package org.example.mvc.controller;

import org.example.model.Contact;
import org.example.services.ContactService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.HttpRequestHandler;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping(path = "/contacts")
public class ContactsController {

    @Autowired
    private ContactService contactService;

    @Autowired
    private Contact contact;

    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView get(ModelAndView modelAndView,
                            @RequestParam(value = "page", required = false) Integer page) {
        return fillModel(modelAndView, page);
    }

    @RequestMapping(method = RequestMethod.POST)
    public ModelAndView add(ModelAndView modelAndView,
                            @ModelAttribute("contact") Contact contact,
                            @RequestParam(value = "page", required = false) Integer page) {
        contactService.addContact(contact);
        return fillModel(modelAndView, page);
    }

    @RequestMapping(method = RequestMethod.POST, params = "contactIdToEdit")
    public ModelAndView edit(ModelAndView modelAndView,
                             @RequestParam("contactIdToEdit") Long contactId,
                             @RequestParam(value = "page", required = false) Integer page) {
        final Contact contactToEdit = contactService.getContacts().stream()
                .filter(contact -> contact.getId().equals(contactId))
                .findAny()
                .orElse(contact);
        return fillModel(modelAndView, page, contactToEdit);
    }

    @RequestMapping(method = RequestMethod.POST, params = "contactId")
    public ModelAndView delete(ModelAndView modelAndView,
                               @RequestParam("contactId") Long contactId,
                               @RequestParam(value = "page", required = false) Integer page) {
        contactService.deleteContact(contactId);
        return fillModel(modelAndView, page);
    }


    public ModelAndView fillModel(ModelAndView modelAndView, Integer page) {
        return fillModel(modelAndView, page, contact);
    }

    public ModelAndView fillModel(ModelAndView modelAndView, Integer page, Contact contact) {
        final int currentPage = page == null ? 1 : page;
        final int pageSize = 5;
        final List<Contact> contacts = contactService.getContacts();

        int from = (currentPage - 1) * pageSize; // 15
        int to = Math.min(currentPage * pageSize, contacts.size()); // ??? // 20
        //contacts size = 16    to: 5 10 15 20->16

        final List<Contact> currentPageContacts = contacts.subList(from, to);


        final List<Integer> pagination = new ArrayList<>();
        for (int i = 0; i < contacts.size(); i += pageSize) {
            pagination.add(i / pageSize + 1);
        }

        modelAndView.addObject("contacts", currentPageContacts);
        modelAndView.addObject("contact", contact);
        modelAndView.addObject("currentPage", currentPage);
        modelAndView.addObject("pagination", pagination);
        return modelAndView;
    }
}
