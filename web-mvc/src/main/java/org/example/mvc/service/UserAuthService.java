package org.example.mvc.service;

import org.example.model.Contact;
import org.example.services.ContactService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserAuthService implements UserDetailsService {

    @Autowired
    private ContactService contactService;

    @Override
    public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {
        //Contact contact = contactService.findByLogin(login);
        Contact contact = contactService.getContacts()
                .stream()
                .filter(c -> c.getLogin().equals(login))
                .findFirst()
                .orElseThrow(() -> new UsernameNotFoundException("user not found"));

        return new User(login, "{noop}" + contact.getPassword(), getAuthorities(contact));
    }

    private List<GrantedAuthority> getAuthorities(Contact contact) {
        List<GrantedAuthority> result = new ArrayList<>();
        switch (contact.getId().intValue() % 3) {
            case 0:
                result.add(new SimpleGrantedAuthority("ROLE_ADMIN"));
                break;
            case 1:
                result.add(new SimpleGrantedAuthority("ROLE_MODERATOR"));
                break;
            case 2:
                result.add(new SimpleGrantedAuthority("ROLE_USER"));
                break;
        }
        return result;
    }
}
