package org.example.spring;

import org.example.spring.config.AnnotationConfig;
import org.example.spring.service.IGreetingService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
//@ContextConfiguration(classes = {AnnotationConfig.class})
@ContextConfiguration("spring-context.xml")
public class SpringTest {

    @Autowired
    private IGreetingService greetingService;

    @Test
    public void test() {
        greetingService.greet();
        System.out.println();
    }
}
