package org.example.spring.model;

import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class PersonTest {

    // Service service = new service() {
    //    @Inject
    //    @Autowired
    //    service  = null???
    // };

    @Test
    public void test() {
        try (ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("spring-context.xml")) {
            final Object person_1 = context.getBean("person_1");
            final Object person_1_2 = context.getBean("person_1");

            final Object person_2 = context.getBean("person_2");
            final Object person_2_2 = context.getBean("person_2");

            final Object person_3 = context.getBean("person_3");
            final Object person_3_2 = context.getBean("person_3");

            final Object person_4 = context.getBean("person_4");
            final Object person_4_2 = context.getBean("person_4");

            final Object person_5 = context.getBean("person_5");
            final Object person_5_2 = context.getBean("person_5");

            System.out.println();
        }

        System.out.println();
    }

}
