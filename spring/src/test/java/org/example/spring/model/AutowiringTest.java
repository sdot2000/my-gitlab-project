package org.example.spring.model;

import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class AutowiringTest {

    @Test
    public void test() {
        try (ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("spring-auto.xml")) {
            final Person person = context.getBean("person", Person.class);
            final Person personByType = context.getBean("personByType", Person.class);
            final Profile profile = context.getBean("profile", Profile.class);
            System.out.println();
        }
    }
}
