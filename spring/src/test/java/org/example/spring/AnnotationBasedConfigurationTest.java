package org.example.spring;

import org.example.spring.config.AnnotationConfig;
import org.example.spring.config.AnnotationModelConfig;
import org.example.spring.model.Person;
import org.junit.Test;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class AnnotationBasedConfigurationTest {

    @Test
    public void test1() {
        try (ConfigurableApplicationContext context = new ClassPathXmlApplicationContext("spring-ab.xml")) {
            final Person person = context.getBean("person", Person.class);
            System.out.println();
        }
    }

    @Test
    public void test2() {
        try (ConfigurableApplicationContext context = new AnnotationConfigApplicationContext(
                AnnotationConfig.class)) {
            final Person person = context.getBean("person", Person.class);
            System.out.println();
        }
    }
}
