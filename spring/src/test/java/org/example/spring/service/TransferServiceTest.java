package org.example.spring.service;

import org.example.spring.model.TransferData;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.aop.framework.ProxyFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.math.BigDecimal;
import java.time.LocalDate;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("spring-aop.xml")
public class TransferServiceTest {

    @Autowired
    private TransferService transferService;

    @Test
    public void test() {
        final TransferService transferService = new TransferService();
        final ProxyFactory proxyFactory = new ProxyFactory();
        proxyFactory.addAdvice(new DataSniffer());
        proxyFactory.setTarget(transferService);
        final TransferService proxyTransferService = (TransferService) proxyFactory.getProxy();

        final TransferData transferData = new TransferData(123456789L, 987654321L,
                new BigDecimal(2000), "Ivanko", LocalDate.now().plusDays(123), 222);

        final Result result = proxyTransferService.doTransfer(transferData);
        System.out.println(result);
    }

    @Test
    public void test2() {
        final TransferData transferData = new TransferData(123456789L, 987654321L,
                new BigDecimal(2000), "Ivanko", LocalDate.now().plusDays(123), 222);
        transferService.doTransfer(transferData);
        System.out.println("-------------");
        transferService.doTransfer();
        System.out.println("-------------");
        transferService.doTransfer(1,2);
    }
}
