package org.example.spring.service;

import org.junit.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class GreetingServiceTest {

    @Test
    public void test() {
        try (ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("spring-context.xml")) {
            final Object greetingService1 = context.getBean("greetingService");
            final IGreetingService greetingService2 = context.getBean("greetingService", IGreetingService.class);
            //final IGreetingService greetingService3 = context.getBean(IGreetingService.class);

            ((IGreetingService) greetingService1).greet();
            greetingService2.greet();
            //greetingService3.greet();

            System.out.println();
        }

        try (final AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext("org.example.spring.service")) {
            final IGreetingService greetingService = context.getBean(IGreetingService.class);
            greetingService.greet();

            final IGreetingService greetingService2 = context.getBean("rus", IGreetingService.class);
            greetingService2.greet();
        }
    }
}
