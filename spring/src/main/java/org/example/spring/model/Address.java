package org.example.spring.model;

import org.example.spring.config.MyConfig;
import org.springframework.stereotype.Component;

@MyConfig
//@Component("address")
public class Address {

    public static final String DEFAULT_COUNTRY = "Belarus";

    private String country;
    private String city;
    private String postcode;
    private String street;

    public Address() {
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }
}
