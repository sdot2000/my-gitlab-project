package org.example.spring.model;

import org.example.spring.config.MyConfig;
import org.example.spring.service.IGreetingService;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@MyConfig
@Component("person")
public class Person { // implements InitializingBean, DisposableBean {

    @Value("#{greetingService}")
    private IGreetingService greetingService;

    @Value("Ivan")
    private String firstName;
    @Value("Ivanov")
    private String lastName;
    @Value("#{address}")
    private Address address;

    private Profile profile;


    public Person() {
    }

    public Person(String firstName) {
        this.firstName = firstName;
    }

    public Person(IGreetingService greetingService) {
        this.greetingService = greetingService;
    }

    public Person(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public static Person getInstance() {
        return new Person();
    }

    public static Person getInstance(String firstName) {
        return new Person(firstName);
    }

    public void before() {
        System.out.println("before");
    }

    public void after() {
        System.out.println("after");
    }

    //@Override
    public void destroy() throws Exception {
        System.out.println("destroy");
    }

    //@Override
    public void afterPropertiesSet() throws Exception {
        System.out.println("afterPropertiesSet");
    }

    public Profile getProfile() {
        return profile;
    }

    public void setProfile(Profile profile) {
        this.profile = profile;
    }

    public IGreetingService getGreetingService() {
        return greetingService;
    }

    public void setGreetingService(IGreetingService greetingService) {
        this.greetingService = greetingService;
    }
}
