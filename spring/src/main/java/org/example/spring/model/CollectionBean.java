package org.example.spring.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

public class CollectionBean {

    private List<String> currencies;

    private Set<String> languages;

    private Collection<String> cars = new ArrayList<>();

    private Map<String, String> citiesInCountry;

    private Properties connectionProps;

    private List<Person> personList;

    private List<String> personNames;

    private List<Integer> numbers;

    private Integer number;

    public CollectionBean() {
    }

    public List<String> getCurrencies() {
        return currencies;
    }

    public void setCurrencies(List<String> currencies) {
        this.currencies = currencies;
    }

    public Set<String> getLanguages() {
        return languages;
    }

    public void setLanguages(Set<String> languages) {
        this.languages = languages;
    }

    public Collection<String> getCars() {
        return cars;
    }

    public void setCars(Collection<String> cars) {
        this.cars = cars;
    }

    public Map<String, String> getCitiesInCountry() {
        return citiesInCountry;
    }

    public void setCitiesInCountry(Map<String, String> citiesInCountry) {
        this.citiesInCountry = citiesInCountry;
    }

    public Properties getConnectionProps() {
        return connectionProps;
    }

    public void setConnectionProps(Properties connectionProps) {
        this.connectionProps = connectionProps;
    }

    public List<Person> getPersonList() {
        return personList;
    }

    public void setPersonList(List<Person> personList) {
        this.personList = personList;
    }

    public List<String> getPersonNames() {
        return personNames;
    }

    public void setPersonNames(List<String> personNames) {
        this.personNames = personNames;
    }

    public List<Integer> getNumbers() {
        return numbers;
    }

    public void setNumbers(List<Integer> numbers) {
        this.numbers = numbers;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }
}
