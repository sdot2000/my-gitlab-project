package org.example.spring.model;

import org.example.spring.service.IGreetingService;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

@Component
public class Profile {

    private IGreetingService greetingService;

    private IGreetingService greetingService2;
    private String login;
    private String password;

    public Profile(IGreetingService greetingService) {
        this.greetingService = greetingService;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public IGreetingService getGreetingService() {
        return greetingService;
    }

    public void setGreetingService(IGreetingService greetingService) {
        this.greetingService = greetingService;
    }
}
