package org.example.spring.model;

import java.math.BigDecimal;
import java.time.LocalDate;

public class TransferData {

    private Long fromCardNumber;
    private Long toCardNumber;
    private BigDecimal amount;
    private String cardholder;
    private LocalDate expireDate;
    private Integer cvv;

    public TransferData(Long fromCardNumber, Long toCardNumber, BigDecimal amount, String cardholder, LocalDate expireDate, Integer cvv) {
        this.fromCardNumber = fromCardNumber;
        this.toCardNumber = toCardNumber;
        this.amount = amount;
        this.cardholder = cardholder;
        this.expireDate = expireDate;
        this.cvv = cvv;
    }

    public Long getFromCardNumber() {
        return fromCardNumber;
    }

    public void setFromCardNumber(Long fromCardNumber) {
        this.fromCardNumber = fromCardNumber;
    }

    public Long getToCardNumber() {
        return toCardNumber;
    }

    public void setToCardNumber(Long toCardNumber) {
        this.toCardNumber = toCardNumber;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getCardholder() {
        return cardholder;
    }

    public void setCardholder(String cardholder) {
        this.cardholder = cardholder;
    }

    public LocalDate getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(LocalDate expireDate) {
        this.expireDate = expireDate;
    }

    public Integer getCvv() {
        return cvv;
    }

    public void setCvv(Integer cvv) {
        this.cvv = cvv;
    }
}
