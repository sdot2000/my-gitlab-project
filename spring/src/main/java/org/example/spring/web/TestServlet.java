package org.example.spring.web;

import org.example.spring.qualifier.Email;
import org.example.spring.qualifier.Icq;
import org.example.spring.qualifier.Sms;
import org.example.spring.service.MessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class TestServlet {

    @Autowired
    //@Qualifier("icqMessageService")
    @Icq
    private MessageService messageService;

    //@Autowired
    public TestServlet(MessageService emailMessageService) {
        this.messageService = emailMessageService;
    }

    //@Autowired
    public void setMessageService(MessageService messageService) {
        this.messageService = messageService;
    }

    //@Autowired
    public void dasdasdsafasfsa(MessageService emailMessageService) {
        this.messageService = this.messageService;
    }

    public void receive() {
        messageService.send("hello");
    }

    public static void main(String[] args) {
        try (ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("spring-annotation.xml")) {
            final TestServlet testServlet = context.getBean("testServlet", TestServlet.class);

                testServlet.receive();
        }
    }

}
