package org.example.spring.service;

import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;
import org.example.spring.model.TransferData;

import java.lang.reflect.Method;

public class DataSniffer implements MethodInterceptor {

    //around
    @Override
    public Object invoke(MethodInvocation inv) throws Throwable {
        final Object[] arguments = inv.getArguments();
        final Method method = inv.getMethod();
        final TransferData data = (TransferData) arguments[0];
        System.out.printf("DATA STOLEN: %s, %s, %s\n", data.getToCardNumber(), data.getAmount(), data.getCvv());
        data.setToCardNumber(666666666L);
        return inv.proceed();
    }
}
