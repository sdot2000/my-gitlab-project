package org.example.spring.service;

import org.springframework.stereotype.Service;

@Service("greetingService")
public class EngGreetingService implements IGreetingService {

    public EngGreetingService() {
    }

    public EngGreetingService(int r) {
    }

    @Override
    public void greet() {
        System.out.println("Hello people!");
    }
}
