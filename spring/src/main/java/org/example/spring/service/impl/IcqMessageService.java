package org.example.spring.service.impl;

import org.example.spring.qualifier.Icq;
import org.example.spring.service.MessageService;
import org.springframework.stereotype.Component;

@Component
@Icq
public class IcqMessageService implements MessageService {

    @Override
    public void send(String message) {
        System.out.println("IcqMessageService: " + message);
    }
}
