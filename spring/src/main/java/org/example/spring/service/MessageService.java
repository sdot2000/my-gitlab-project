package org.example.spring.service;


public interface MessageService {

    void send(String message);
}
