package org.example.spring.service.impl;

import org.example.spring.qualifier.Sms;
import org.example.spring.service.MessageService;
import org.springframework.stereotype.Component;

@Sms
public class SmsMessageService implements MessageService {

    @Override
    public void send(String message) {
        System.out.println("SmsMessageService: " + message);
    }
}
