package org.example.spring.service;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.example.spring.model.TransferData;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class AspectJDataSniffer {

    @Pointcut("execution(* org.example.spring.service.TransferService.doTransfer(..))")
    public void doTransfer() { }

    @Pointcut("execution(* org.example.spring.service.TransferService.doTransfer(..)) && args(data)")
    public void doTransferWithArgs(TransferData data) { }

    @Before(value = "doTransfer()")
    public void before() {
        System.out.println("BEFORE");
    }

    @After(value = "doTransfer()")
    public void after() {
        System.out.println("AFTER");
    }

    @AfterThrowing("doTransfer()")
    public void afterThrowing() {
        System.out.println("AFTER THROWING");
    }

    @Before(value = "doTransferWithArgs(data)", argNames = "data")
    public void beforeArgs(TransferData data) {
        System.out.println("BEFORE WITH ARGS: " + data.getCardholder());
    }

    @Around("doTransfer()")
    public Object around(ProceedingJoinPoint point) throws Throwable {
        System.out.println("AROUND");
        return point.proceed();
    }


}
