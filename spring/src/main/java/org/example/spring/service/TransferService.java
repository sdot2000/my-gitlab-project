package org.example.spring.service;

import org.example.spring.model.TransferData;
import org.springframework.stereotype.Service;

import java.util.Random;

@Service
public class TransferService {

    public Result doTransfer() {
        return Result.FAILED;
    }

    public Result doTransfer(int a, int b) {
        throw new IllegalArgumentException();
    }

    public Result doTransfer(TransferData data) {
        System.out.println("TransferService - START");
        final int i = new Random().nextInt(3);
        Result result;
        switch (i) {
            case 1:
                result = Result.OK;
                break;
            case 2:
                result = Result.NO_FUNDS;
                break;
            case 0:
            default:
                result = Result.FAILED;
                break;
        }

        System.out.printf("RESULT: %s, TO_CARD: %s, AMOUNT: %s\n", result, data.getToCardNumber(), data.getAmount());

        System.out.println("TransferService - END");
        return result;
    }


}

enum Result {
    OK,
    NO_FUNDS,
    FAILED
}
