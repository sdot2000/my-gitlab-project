package org.example.spring.service.impl;

import org.example.spring.qualifier.Email;
import org.example.spring.service.MessageService;
import org.springframework.stereotype.Service;

@Service
@Email
public class EmailMessageService implements MessageService {
    @Override
    public void send(String message) {
        System.out.println("EmailMessageService:" + message);
    }
}
