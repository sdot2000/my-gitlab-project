package org.example.spring.service;

import org.springframework.stereotype.Service;

@Service(value = "ru")
public class RuGreetingService implements IGreetingService {

    @Override
    public void greet() {
        System.out.println("Привет народ!");
    }
}
