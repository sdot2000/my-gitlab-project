package org.example.spring.config;

import org.example.spring.model.Person;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

//@Configuration
public class AnnotationModelConfig {

    @Bean
    public Person defaultPerson() {
        return new Person();
    }
}
