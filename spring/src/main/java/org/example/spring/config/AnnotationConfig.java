package org.example.spring.config;

import org.example.spring.model.Person;
import org.example.spring.service.MessageService;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ComponentScan.Filter;
import org.springframework.context.annotation.ComponentScans;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

@Configuration
@ComponentScans({
        @ComponentScan(
                basePackages = "org.example.spring.model",
                //todo: check this filter.
                includeFilters = @Filter(type = FilterType.ANNOTATION, value = MyConfig.class)
        ),
        @ComponentScan(
                basePackages = "org.example.spring.service",
                excludeFilters = @Filter(type = FilterType.ASSIGNABLE_TYPE , classes = MessageService.class)
        )
})
@Import(AnnotationModelConfig.class)
public class AnnotationConfig {

        @Bean
        @Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
        public Person defaultPerson2() {
                return new Person();
        }
}
