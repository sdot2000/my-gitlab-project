package org.example.spring;

import org.example.spring.model.Person;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.util.List;

public class ReflectionTest {

    public ReflectionTest() {

    }

    public void doSmth() {
        System.out.println("DO");
    }

    public static void main(String[] args) throws Exception {
        final Constructor<?> constructor = ReflectionTest.class.getConstructors()[0];
        final Object o = constructor.newInstance();
        final Method[] methods = o.getClass().getMethods();
        final Method method = methods[0];
        method.invoke(o);
        //System.out.println();

        new ReflectionTest().doSmth();
    }

    //@Transactional
    public List<Person> getPersons() {
        return null;
    }

    public List<Person> getPersons(int a, int b) {
        return null;
    }

    public List<Person> proxyGetPersons(int a, int b) {
        //start tx

        System.out.println(a + " " + b);
        final List<Person> persons = getPersons(a, b);

        //newPersons = persons.stream().filter( person -> person.getRole().equals("USER"))
        //end tx

        return persons;

    }

}


