CREATE TABLE user_role (
    id INT NOT NULL PRIMARY KEY AUTO_INCREMENT, -- PK
    name VARCHAR(45) NOT NULL UNIQUE,
    admin CHAR(1) NOT NULL
);

CREATE TABLE contact (
    id INT NOT NULL PRIMARY KEY AUTO_INCREMENT, -- PK
    login VARCHAR(45) NOT NULL UNIQUE,
    password VARCHAR(45) NOT NULL,
    first_name VARCHAR(45),
    last_name VARCHAR(45),
    user_role_id INT NOT NULL, -- FK
    CONSTRAINT fk_contact_user_role FOREIGN KEY (user_role_id) REFERENCES user_role(id)
);

CREATE TABLE tutor (
    id INT NOT NULL PRIMARY KEY AUTO_INCREMENT, -- PK
    title VARCHAR(45) NOT NULL,
    contact_id INT NOT NULL, -- FK
    CONSTRAINT fk_tutor_contact FOREIGN KEY (contact_id) REFERENCES contact(id)
);

CREATE TABLE lecturer (
    id INT NOT NULL PRIMARY KEY AUTO_INCREMENT, -- PK
    tutor_id INT NOT NULL, -- FK
    CONSTRAINT fk_lecturer_tutor FOREIGN KEY (tutor_id) REFERENCES tutor(id)
);

CREATE TABLE trainer (
    id INT NOT NULL PRIMARY KEY AUTO_INCREMENT, -- PK
    tutor_id INT NOT NULL, -- FK
    CONSTRAINT fk_trainer_tutor FOREIGN KEY (tutor_id) REFERENCES tutor(id)
);

CREATE TABLE student (
    id INT NOT NULL PRIMARY KEY AUTO_INCREMENT, -- PK
    studentIdentifier INT NOT NULL,
    contact_id INT NOT NULL, -- FK
    CONSTRAINT fk_student_contact FOREIGN KEY (contact_id) REFERENCES contact(id)
);

CREATE TABLE course (
    id INT NOT NULL PRIMARY KEY AUTO_INCREMENT, -- PK
    name VARCHAR(45) NOT NULL,
    details VARCHAR(45) NOT NULL
);

CREATE TABLE course_class (
    id INT NOT NULL PRIMARY KEY AUTO_INCREMENT, -- PK
    details VARCHAR(45) NOT NULL,
    course_id INT NOT NULL, -- FK
    lecturer_id INT NOT NULL, -- FK
    trainer_id INT NOT NULL, -- FK
    CONSTRAINT fk_course_class_course FOREIGN KEY (course_id) REFERENCES course(id),
    CONSTRAINT fk_course_class_lecturer FOREIGN KEY (lecturer_id) REFERENCES lecturer(id),
    CONSTRAINT fk_course_class_trainer FOREIGN KEY (trainer_id) REFERENCES trainer(id)
);

CREATE TABLE course_class_student (
    id INT NOT NULL PRIMARY KEY AUTO_INCREMENT, -- PK
    mark INT NOT NULL,
    course_class_id INT NOT NULL, -- FK
    student_id INT NOT NULL, -- FK
    CONSTRAINT fk_course_class_student_course_class FOREIGN KEY (course_class_id) REFERENCES course_class(id),
    CONSTRAINT fk_course_class_student_student FOREIGN KEY (student_id) REFERENCES student(id)
);

--

INSERT INTO user_role (name, admin)
VALUES ('Admin', 'Y'),
       ('Tutor', 'N'),
       ('Student', 'N'),
       ('Sponsor', 'N'),
       ('Visitor', 'N');

INSERT INTO course (name, details)
VALUES ('Mathematics', 'Mathematics'),
       ('Physics', 'Physics'),
       ('Chemistry', 'Chemistry'),
       ('Biology', 'Biology'),
       ('Geography', 'Geography');

INSERT INTO contact
(login, password, first_name, last_name, user_role_id)
VALUES
('admin', 'admin', 'Ivan', 'Bold', 1),
('tutor1', 'tutor1', 'Gregor1', 'Minchev1', 2),
('tutor2', 'tutor2', 'Gregor2', 'Minchev2', 2),
('student1', 'student1', 'Vasili1', 'Terkin1', 3),
('student2', 'student2', 'Vasili2', 'Terkin2', 3),
('student3', 'student3', 'Vasili3', 'Terkin3', 3);

INSERT INTO tutor (title, contact_id)
VALUES ('Tutor1', 2),
       ('Tutor2', 3);

INSERT INTO lecturer (tutor_id)
VALUES (1);

INSERT INTO trainer (tutor_id)
VALUES (2);

INSERT INTO student (studentIdentifier, contact_id)
VALUES (1, 4),
       (2, 5),
       (3, 6);

INSERT INTO course_class (details, course_id, lecturer_id, trainer_id)
VALUES ('Mathematics', 1, 1, 1),
       ('Physics', 2, 1, 1),
       ('Chemistry', 3, 1, 1),
       ('Biology', 4, 1, 1),
       ('Geography', 5, 1, 1);

INSERT INTO course_class_student (mark, course_class_id, student_id)
VALUES (10, 1, 1),
       (9, 1, 2),
       (8, 1, 3),
       (7, 2, 1),
       (6, 2, 2),
       (5, 3, 1),
       (6, 3, 3),
       (7, 4, 2),
       (8, 5, 2);
