select email, name, count(*)
from employee
group by email, name
having count(*) > 1;

--
select e.id, e.email, e.name
from employee e
         right join
     (select email, name, count(*)
      from employee
      group by email, name
      having count(*) > 1) as e2 on e.name = e2.name AND e.email = e2.email;

--
select d.name, avg(e.salary)
from department d
         left join employee e on d.id = e.dep_id
group by d.id

--

select d.name, case when avg(e.salary) is null then 'no employee' else avg(e.salary) end as salary
from employee AS e
         right join department AS d on d.id = e.dep_id
group by d.id;

--

select n.name, e.name, maxsal
from employee e
         right join (
    select d.id, d.name, max(e.salary) as maxsal
    from department d
             left join employee e on d.id = e.dep_id
    group by d.id
    limit 3
) as n on e.salary = n.maxsal and n.id = e.dep_id
group by n.name, maxsal;